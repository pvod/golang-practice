package main

import "github.com/dinever/golf"

func mainHandler(ctx *golf.Context) {
	ctx.Send("Hello World!")
}

func pageHandler(ctx *golf.Context) {
	ctx.Send("Page: " + ctx.Param("page"))
}

func main() {
	app := golf.New()
	app.Get("/", mainHandler)
	app.Get("/p/:page/", pageHandler)
	app.Run(":9000")
}
